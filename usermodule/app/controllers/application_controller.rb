class ApplicationController < ActionController::Base
  helper_method :current_user
  
  def current_user
    @current_user ||= User.find_by_auth_token!(cookies[:auth_token]) if cookies[:auth_token]
    #if session[:user_id]
    # @current_user ||= User.find(session[:user_id])
    #else
    #  @current_user = nil
    #end
  end
end
