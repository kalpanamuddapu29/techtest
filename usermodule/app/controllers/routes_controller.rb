class RoutesController < ApplicationController
  
  require 'rest-client'
  require 'json'  

  def index
  	search = params[:search]
    url = 'http://178.217.134.14/journeys/api/1/routes'
    response = RestClient.get(url)
    if(search)
      @routes1 = JSON.parse(response.body)
      #@routes['body'].each { |h| h.delete() }
      @routes = @routes1['body'].map { |h| h['name'] == 'Vatiala - Suupantori'; h }
      #@routes1["body"].each do |route|
  	  #	puts route if route["name"] == 'Vatiala - Suupantori'
  	  #end
  	else
  	  @routes = JSON.parse(response.body)
    end
  end	

  def show	
  	url = params[:url]
  	#@url = Route.find(params[:url])
  	response = RestClient.get(url)
    @routes = JSON.parse(response)
  end

  def showstops	
  	url = params[:url]
  	#@url = Route.find(params[:url])
  	response = RestClient.get(url)
    @stops = JSON.parse(response)
  end

  def showmap
  end

end
