# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version

  ruby 2.5.5
  Rails 6.0.0

* System dependencies

* Configuration

* Database creation
  1) Create database 'techtest'
  2) Migrate users table
     >>> rails db:migrate

* Database initialization

* How to run the test suite

 rails db:migrate RAILS_ENV=test
 

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

  1) Run the server
     >>> rails s

  2) Navigate to the following URL on browser
     http://<>:3000

  3) The application basically does the following 
      i) A visitor can create an account.	
	  ii) A user can login.
	  iii) A user can logout.
      iv) A user can modify his/her profile.
      v) A user can modify the password.
      vi) A user should be able to reset a password via email.
      vii) A user should be able to show multiple routes.
      viii) A user should be able to view the expected arrival time and departure time at a stop

