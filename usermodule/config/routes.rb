Rails.application.routes.draw do
  root 'home#index'

  resources :users
  resources :sessions, only: [:new, :create, :destroy]
  resources :password_resets
  #resources :routes
  
  get 'signup', to: 'users#new', as: 'signup'
  get 'login', to: 'sessions#new', as: 'login'
  get 'logout', to: 'sessions#destroy', as: 'logout'
  get 'routes', to: 'routes#index', as: 'routes'
  get 'details', to: 'routes#show', as: 'details'
  get 'stopdetails', to: 'routes#showstops', as: 'stopdetails'
  get 'showmap', to: 'routes#showmap', as: 'showmap'

  #get '/routes/show/:url' => 'routes#show'

end
